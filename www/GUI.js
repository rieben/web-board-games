
{
	let state = true
	document.getElementById('help').onclick = () => {
		state = !state
		if(state) {
			document.getElementById('help').innerHTML="help "
			document.getElementById('helper').style.display="none"
		} else {
			document.getElementById('help').innerHTML="close"
			document.getElementById('helper').style.display="block"
		}
	}
}

{
	let state = true
	document.getElementById('console').onclick = () => {
		state = !state
		if(state) {
			document.getElementById('console').innerHTML="console"
			document.getElementById('console-interface').style.display="none"
		} else {
			document.getElementById('console').innerHTML="close  "
			document.getElementById('console-interface').style.display="block"
		}
	}
}


let loginToggle
{
	let state = true
	loginToggle = s => {
		state = s || !state
		if(state) {
			document.getElementById('login').innerHTML="login"
			document.getElementById('login-interface').style.display="none"
		} else {
			document.getElementById('login').innerHTML="close"
			document.getElementById('login-interface').style.display="block"
			document.getElementById('login-input').focus()
		}
	}
	document.getElementById('login').onclick = () => { loginToggle() }

	document.getElementById("login-input").addEventListener("keyup", function(event) {
		if(event.keyCode === 13) {
			event.preventDefault()
			let user = document.getElementById("login-input").value
			if(user.length == 0)
				return
			loginToggle(false)
			login(user)
		}
	})
	document.getElementById("logout-input").onclick = () => {
		loginToggle(false)
		logout()
	}
}
