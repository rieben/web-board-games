"use strict";

// let socket = new Socket('ws://127.0.0.1:8081')
let socket = new Socket('ws://rainbowdash.gnugen.ch:64175/')

////////////////////////////////////////////////////////////////////

let log = msg => document.getElementById('console-out').innerHTML += msg + '\n'

socket.bind('print',msg => {log(msg.msg);console.log(msg.msg)})

let me

let login = user => socket.send({ do:"login", user:user, })
socket.bind("login",msg => {
	me = msg.user
	document.getElementById('username').innerHTML = me
})

let logout = () => { if(me) socket.send({do:"logout"}) }
socket.bind("logout",msg => {
	me = undefined
	document.getElementById('username').innerHTML = ""
})

socket.bind("userListChange",msg => {
	document.getElementById('userlist').innerHTML =
	"<span style='color:grey'>users("+msg.users.length+")</span><br>"
	+ msg.users.join("<br>")
})

////////////////////////////////////////////////////////////////////

let tb = {};

tb.join = () => socket.send({
	do:"timebomb",
	game:'default',
	gameDo:'join',
})

tb.quit = () => socket.send({
	do:"timebomb",
	game:'default',
	gameDo:'quit',
})

tb.start = () => socket.send({
	do:"timebomb",
	game:'default',
	gameDo:'start',
})

tb.list = () => socket.send({
	do:"timebomb",
	game:'default',
	gameDo:'list',
})

tb.cut = (player,index) => socket.send({
	do:"timebomb",
	game:'default',
	gameDo:'cut',
	gameDoPlayer:player,
	gameDoIndex:index,
})

tb.show = () => socket.send({
	do:"timebomb",
	game:'default',
	gameDo:'show',
})

// setTimeout(() => {
// 	tb.start()

// 	login("p1")
// 	tb.join()
// 	tb.start()

// 	login("p2")
// 	tb.join()
// 	tb.start()

// 	login("p3")
// 	tb.join()
// 	tb.start()

// 	login("p4")
// 	tb.join()

// 	login("p5")
// 	tb.join()
// 	tb.quit()
// 	tb.list()
// 	tb.join()
// 	tb.list()


// 	tb.start()
// 	tb.start()
// 	tb.start()
// 	tb.start()
// 	tb.start()
// 	tb.start()
// 	tb.start()
// 	tb.start()

// },1000)

