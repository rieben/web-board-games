
let Socket = function(socketAddress) {
	let connection = new WebSocket(socketAddress)

	connection.onopen = event => console.log("connected")
	connection.onerror = error => console.log("connexion failed")

	setInterval(() => {
		if (connection.readyState !== 1) {
			document.getElementById("header").style.background="red"
			document.getElementById("error").style.background="yellow"
			document.getElementById("error").style.color="red"
			document.getElementById("error").style.paddingLeft="20px"
			document.getElementById("error").style.paddingRight="20px"
			document.getElementById("error").innerHTML="connexion lost"
		}
	}, 2000)

	this.send = msg => connection.send(JSON.stringify(msg))

	let actions = {}
	this.bind = (actionName,action) => actions[actionName] = action
	connection.onmessage = msg => {
		msg = JSON.parse(msg.data)
		for( let actionName in actions )
			if(msg.do && msg.do == actionName)
				actions[actionName](msg)
	}
}
