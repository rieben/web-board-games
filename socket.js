let consoleCol = require('./consoleColor.js')
let logger = require('./logger.js')

let Users = function() {
	let users = []
	this.list = () => users
	this.login = user => {
		users=users.filter(u=>u!=user)
		users.push(user)
	}
	this.logout = user => {
		users = users.filter(u=>u!=user)
	}
}

let users = new Users()
let allSocket = []

let Socket = function(wsServerReq) {
	let connection = wsServerReq.accept(null, wsServerReq.origin) 

	let user

	this.login = usr => {
		users.login(usr)
		user=usr
		this.send({do:"login",user:usr})
	}

	this.logoutHelper = () => {
		users.logout(user)
		user=undefined
		this.send({do:'logout'})
	}

	this.logout = () => {
		let userToRemove = this.getUser()
		allSocket
			.filter(s => s.getUser() == userToRemove)
			.map(s => s.logoutHelper())
	}

	this.getUser = () => user

	this.log = msg => logger.log( consoleCol.blue   + "wsok" + consoleCol.normal + " [" + consoleCol.blue + wsServerReq.key + consoleCol.normal + (user?("|"+consoleCol.blue+user+consoleCol.normal):"")+"] " + msg)
	this.err = msg => logger.err( consoleCol.blue   + "wsok " + consoleCol.normal + msg)

	this.send = msg => connection.send(JSON.stringify(msg))

	let actions = {}
	this.bind = (actionName,action) => actions[actionName] = action
	this.trigger = (actionName,msg) => actions[actionName](msg)

	connection.on('message', msg => {
		msg = JSON.parse(msg.utf8Data)
		for( let actionName in actions )
			if(msg.do && msg.do == actionName)
				actions[actionName](msg)
	})

	allSocket.push(this)

	this.notifyUserListChange = () => allSocket.map(sok => sok.send({do:"userListChange",users:users.list()}))
	this.notifyUserListChange()

	connection.on('close', connection => {
		allSocket.filter( e => e!=this )
		this.log( "disconnected" )
	})

	this.bind('login', msg => {
		this.login(msg.user)
		this.log("login")
		this.notifyUserListChange()
	})

	this.bind('logout', msg => {
		this.log("logout")
		this.logout()
		this.notifyUserListChange()
	})

	this.log( "connected" )
}

exports.Socket = Socket
