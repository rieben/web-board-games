let shuffle = require('./tools.js').shuffle

let TimeBomb = function() {

	let players = []
	this.list = () => players

	let join = user => {
		if(!user) return
		if(players.filter(u => u==user).length == 0) players.push(user)
		return "join"
	}
	let quit = user => {
		players = players.filter(u => u!=user)
		return "quit"
	}
	this.join = join
	this.quit = quit

	let distribuer = cards => {
		gamedata.cards = {}
		for(let p in players) {
			gamedata.cards[players[p]] = []
		}
		let i = 0
		while(cards.length) {
			gamedata.cards[players[i % players.length]].push(cards.pop())
			i++
		}
	}

	let good='gentil'
	let evil='méchant'

	let Card_filMerde = 'fil de merde'
	let Card_filStyle = 'file stylé'
	let Card_bigben   = 'big ben'

	let gamedata

	let start = () => {

		if(players.length<4) {
			return ("need " + (4-players.length) + " more players")
		}
		if(players.length>8) {
			return ("too many players")
		}

		this.join = () => "cannot join"
		this.quit = () => "cannot quit"

		gamedata = {}
		gamedata.roles = {}

		// assign roles
		{
			let roles
				if(players.length==4 || players.length==5)
					roles = (new Array(3).fill(good)).concat(new Array(2).fill(evil))
				if(players.length==6)
					roles = (new Array(4).fill(good)).concat(new Array(2).fill(evil))
				if(players.length==7 || players.length==8)
					roles = (new Array(5).fill(good)).concat(new Array(3).fill(evil))
			shuffle(roles)
			for(let i in players) {
				gamedata.roles[players[i]] = roles[i]
			}
		}

		let cards = (new Array(players.length).fill(Card_filStyle))
		.concat(new Array(4*players.length-1).fill(Card_filMerde))
		.concat([Card_bigben])

		shuffle(cards)
		distribuer(cards)


		shuffle(players)
		gamedata.pince=players[0]

		gamedata.defausse = []

		this.start = () => ("already started")


		return ("game started")

	}
	this.start = start

	this.show = user => {
		// console.log(gamedata)

		let playersHand = {}
		for(let player of players) {
			playersHand[player] = gamedata.cards[player].length
		}

		return {
			user:user,
			role:gamedata.roles[user],
			cards:gamedata.cards[user],
			pince:gamedata.pince,
			playersHand:playersHand,
			defausse:gamedata.defausse.sort()
		}
		// console.log("la défausse contient " + gamedata.defausse.length + " cartes dont " + gamedata.defausse.filter(c=>c==Card_filStyle).length + " fils")
	}

	this.cut = (user,targetUser,index) => {
		if(user==targetUser) {
			return "tu ne peux pas couper sur toi même"
		}
		if(user!=gamedata.pince) {
			return "tu n'as pas la pince"
		}
		if(!gamedata.cards[targetUser]) {
			return (targetUser + " n'existe pas dans cette partie")
		}
		if(!gamedata.cards[targetUser][index]) {
			return (targetUser + " n'a que " + gamedata.cards[targetUser].length + " cartes")
		}
		let card = gamedata.cards[targetUser].splice(index,1)
		gamedata.pince = targetUser
		gamedata.defausse.push(card)
		if(card==Card_bigben) {
			return ("BOUM !")
		}

		let cardLeft = 0
		for(let player of players)
			cardLeft += gamedata.cards[player].length

		if(cardLeft == players.length) {
			return ("GAME END, les méchants gagnent")
		}

		if(gamedata.defausse.length % players.length == 0 ) {
			// shuffle et redistribuer
			let cards = []
			for(let player of players) {
				cards = cards.concat(gamedata.cards[player])
				gamedata.cards[player] = []
			}
			shuffle(cards)
			distribuer(cards)
		}
	}
}

exports.TimeBomb = TimeBomb