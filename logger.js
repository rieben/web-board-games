
let dateFormat = require('dateformat')
let consoleCol = require('./consoleColor.js')

let getTimeStr = () => dateFormat(new Date(), "yyyy-mm-dd h:MM:ss")
let log = msg => console.log( consoleCol.yellow + getTimeStr() + consoleCol.normal + " " + msg)
let err = msg => console.log( consoleCol.red + getTimeStr() + consoleCol.normal + " " + msg)

exports.log = log
exports.err = err
