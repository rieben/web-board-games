"use strict"

// ================
// init + args

process.title = 'web-board-game'

let port = parseInt(process.argv[2]) || 8081

require('readline')
	.createInterface({input: process.stdin,output: process.stdout,})
	.on('line', line => { try { eval("console.log("+line+")") } catch(e) {console.log(e)} })

// ============
// dependencies

let Socket = require("./socket.js").Socket
let createHTTPServer = require("./http.js").createHTTPServer
let webSocketServer = require('websocket').server

// ============
// server setup

let server = createHTTPServer(port)

// ============
// game setup

let TimeBomb = require('./games/timebomb.js').TimeBomb

let games = {}
games.timebomb = {}
games.timebomb['default'] = new TimeBomb()

// ============
// websocket setup

let wsServer = new webSocketServer({httpServer: server})
wsServer.on('request', request => {

	let socket = new Socket(request)

	socket.bind('timebomb', msg => {
		let tb = games.timebomb[msg.game]
		if (!tb) games.timebomb[msg.game] = new TimeBomb()

		if(msg.gameDo == 'join') {
			socket.log('timebomb['+msg.game+'] join')
			socket.send({do:'print',msg:tb.join(socket.getUser())})
		}

		if(msg.gameDo == 'quit') {
			socket.log('timebomb['+msg.game+'] quit')
			socket.send({do:'print',msg:tb.quit(socket.getUser())})
		}

		if(msg.gameDo == 'start') {
			socket.log('timebomb['+msg.game+'] start')
			socket.send({do:'print',msg:tb.start()})
		}

		if(msg.gameDo == 'list') {
			socket.send({do:'print',msg:tb.list()})
		}

		if(msg.gameDo == 'cut') {
			// socket.log('timebomb['+msg.game+'] start')
			socket.send({do:'print',msg:tb.cut(socket.getUser(),msg.gameDoPlayer,msg.gameDoIndex)})
		}

		if(msg.gameDo == 'show') {
			socket.log('timebomb['+msg.game+'] show')
			socket.send({do:'print',msg:tb.show(socket.getUser())})
		}

	})

})
